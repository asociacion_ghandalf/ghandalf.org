+++
title = "Actividades"
+++

## 2018

* Sesións de difusión sobre OpenStreetMap e Software Libre, en institucións de Ensinanza Primaria e Secundaria

## 2017

* Organización do primeiro Drone Day en Galicia
* Colaboración na organización da GeocampES
* Sesións de difusión sobre OpenStreetMap e Software Libre, en institucións de Ensinanza Primaria e Secundaria

## 2016

* Colaboración na organización do PyDay Galicia
* Colaboración na organización da GeocampES
* Colaboración na organización e celebración da quinta edición da OSHWDEM (Open Source Hardware Demostration)
* Sesións de difusión sobre OpenStreetMap e Software Libre, en institucións de Ensinanza Primaria e Secundaria
* Creación da web do libro Máster Software Libre en Galicia, ano II

## 2015

* Colaboración na organización da GeocampES
* Colaboración na organización e celebración da cuarta edición da OSHWDEM (Open Source Hardware Demostration)
* Sesións de difusión sobre Software Libre, en institucións de Formación Profesional e Ensinanza Primaria e Secundaria
* Distribución de exemplares impresos dos libros “Creando Software Libre”, “Máster Software Libre en Galicia,ano I” e “Máster Software Libre en Galicia, ano II“

## 2014

* Colaboración na organización da GeocampES
* Colaboración na organización e celebración da terceira edición da OSHWDEM (Open Source Hardware Demostration)
* Sesións de difusión sobre Software Libre, en institucións de Formación Profesional e Ensinanza Primaria e Secundaria
* Distribución de exemplares impresos dos libros “Creando Software Libre”, “Máster Software Libre en Galicia,ano I” e “Máster Software Libre en Galicia, ano II”

## 2013

* Colaboración na organización da GeocampES
* Colaboración na organización do primeiro QGIS Weekend
* Organización e celebración da segunda edición da OSHWDEM (Open Source Hardware Demostration)
* Sesións de difusión sobre Software Libre, información xeográfica e OSM en institucións de Formación Profesional

## 2012

* Colaboración na organización da GUADEC 2012
* Distribución de exemplares impresos dos libros “Creando Software Libre” e “Máster Software Libre en Galicia, ano I“
* Sesións de difusión sobre Software Libre en institucións de Ensinanza Primaria, Secundaria e Formación Profesional
* Organización e celebración dunha xornada de OSHW (Open Source Hardware)
* Revisión normativa, edición en formato papel e impresión dunha tirada dun libro en galego sobre software libre: Máster Software Libre en Galicia, ano II

## 2011

* Organización dunha GNOME 3 Launch Party en Coruña
* Revisión normativa, edición en formato papel e impresión dunha tirada dun libro en galego sobre software libre: Máster Software Libre en Galicia, ano I
* Realización dunha Xornada sobre Software Libre e licenzamento no IES Xulián Magariños
* Distribución de exemplares impresos do libro “Creando Software Libre“

## 2010

* Colaboración na organización da DudesConf e da Guadec Hispana
* Edición e impresión en papel dunha tirada do libro “Creando Software Libre“
* Sesións de difusión sobre Software Libre en institucións de Ensinanza Primaria e Secundaria
* Realización de duas bolsas para o desenvolvemento de PFCs con Software Libre
* Colaboración na organización e participación na Libre Software World Conference

## 2009

* Obradoiro de Software Libre no Colexio de Enxeñeiros Industriais de Galicia
* Proxecto de traducción colaborativa do libro Producing Open Source Software