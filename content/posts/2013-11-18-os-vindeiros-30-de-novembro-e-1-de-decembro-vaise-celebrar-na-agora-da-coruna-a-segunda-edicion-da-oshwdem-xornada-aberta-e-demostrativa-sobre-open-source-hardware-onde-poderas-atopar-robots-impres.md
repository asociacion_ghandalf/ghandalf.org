---
title: "Os vindeiros 30 de novembro e 1 de decembro vaise celebrar na Ágora da Coruña a segunda edición da OSHWDEM, xornada aberta e demostrativa sobre Open Source Hardware, onde poderás atopar robots, impresoras 3D e electrónica para todos os públicos"
date: "2013-11-18"
categories: 
  - "eventos-publicos"
tags: 
  - "arduino"
  - "electronica"
  - "hardware-libre"
  - "impresoras-3d"
  - "robotica"
  - "software-libre"
---

- É un evento aberto e gratuíto de carácter tecnolóxico cun formato innovador e participativo, centrado no Open Source Hardware (Hardware Libre)
- Dá continuidade ás edicións anteriores, que foron todo un éxito de público, e a eventos similares celebrados ao longo de toda España
- Promovida pola Asociación GHANDALF, e organizada pola asociación BricoLabs, na edición 2013 ampliamos tanto os contidos do evento como o aforo e maila duración do mesmo

Se estás interesado no Open Source Hardware, en Arduino, nas impresoras 3D, en escáneres de obxectos reais, na montaxe de circuítos electrónicos, nos robots, arte dixital, software libre embebido, electrónica en xeral, etc. tes unha cita ineludible os vindeiros 30 de novembro e 1 de Decembro no Centro Cultural Ágora da Coruña.

Como a edición pasada da OSHWDEM foi todo un éxito, recibindo máis do dobre de asistentes dos esperados e obrigando a pechar as inscricións antes do previsto para non superar o aforo do local, este ano ampliamos tanto os contidos do evento como o aforo dispoñible e maila duración do mesmo.

Na OSHWDEM imos combinar charlas divulgativas con espazos demostrativos, abordando diferentes temáticas relacionadas co Hardware Libre: impresoras 3D, Raspberry Pi, Arduino, montaxe de circuítos electrónicos, arte dixital, deseño 3D con ferramentas libres, robótica, etc. Tamén faremos obradoiros de montaxe de circuítos electrónicos e deseño 3D, para que os asistentes poidades meter as mans na masa.

Na organización da OSHWDEM estamos a traballar Asociacións Galegas de Usuarios de Software Libre e de Hardware Libre, como GHANDALF e BricoLabs, contando coa colaboración de empresas do sector como Bricogeek que farán que ninguén marche da xornada coas mans baleiras, e incluso haberá agasallos para os asistentes máis participativos.

Ao longo da xornada, así como durante o xantar, contaremos con espazos de networking coa finalidade de poñer en contacto aos diferentes actores interesados no Hardware Libre. As empresas, administracións, universidades ou mesmo profesionais independentes, interesados no Open Source Hardware poden atopar na OSHWDEM proxectos do seu interese, ou incluso contar con un espazo para amosar os proxectos nos que están a traballar. Contactade connosco a través do correo [info@oshwdem.org](mailto:info@oshwdem.org) e vemos como colaborar.

A entrada para a OSHWDEM é de balde, aínda que para xestionar o aforo e para participar nos concursos é preciso que te inscribas previamente na páxina do evento a través desta ligazón:  [http://oshwdem.org/inscripcion](http://oshwdem.org/inscripcion)  Para estar ao tanto de todas as novidades, tes á túa disposición tanto a web oficial oshwdem.org como a conta de twitter [@OSHWDEM](https://twitter.com/OSHWDEM) así como o enderezo de correo electrónico info@oshwdem.org

Podes ver fotos da edición 2012 en: [http://oshwdem.org/2012/12/08/y-se-lio-la-del-pulpo/](http://oshwdem.org/2012/12/08/y-se-lio-la-del-pulpo/)

Podes ver a cobertura da TVG da edición 2012 en: [http://www.crtvg.es/informativos/xornada-aberta-sobre-hardware-libre-en-a-coruna-466945#.UMJdjpPjlEB](http://www.crtvg.es/informativos/xornada-aberta-sobre-hardware-libre-en-a-coruna-466945#.UMJdjpPjlEB)
