---
title: "A GUADEC Hispana volve a A Coruña"
date: "2010-05-30"
categories: 
  - "eventos-publicos"
---

Este ano a [GUADEC Hispana](http://2010.guadec.es) volve a A Coruña. Esta edición, a septima xa, celebrarase os dias 22 e 23 de xullo na Facultade de Informática da Universidade da Coruña. Será unha boa oportunidade de compartir horas con desenvolvedores e xente importante da comunidade [GNOME](http://es.gnome.org) hispano falante así como coñecer de preto as novidades e o estado do proxecto.

GHANDALF estará presente nesta edición coma colaborador da man de [GPUL](http://gpul.org/) e [GNOME Hispano](http://www.gnomehispano.org/), asociacións organizadoras do evento.

Se a tua empresa, asociación ou institución está interesada en botar unha man ou patrocinar o evento podes poñerte en contacto cos organizadores a través do correo "chair (arroba) guadec (punto) es".

A [petición de contribucións](http://2010.guadec.es/guadec/peticion_de_ponencias) está xa aberta e permanecerá ata o 14 de xullo de 2010. Enviade a vosa o antes posible.
