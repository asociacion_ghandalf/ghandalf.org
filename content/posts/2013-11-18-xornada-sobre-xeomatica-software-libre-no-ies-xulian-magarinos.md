---
title: "Xornada sobre xeomática e software libre no IES Xulián Magariños"
date: "2013-11-18"
categories: 
  - "formacion"
---

Este martes 19 de novembro a Asociación GHANDALF vai impartir unha xornada de divulgación e formación dirixida aos alumnos dos Ciclos Superiores de Informática do IES Xulián Magariños de Negreira.

A temática da devandita xornada vaise centrar nos Sistemas de Información Xeográfica baseados en  Software Libre, tendo como obxectivo amosarlle a panorámica xeral deste ámbito e a súa relación co Software Libre aos alumnos dos ciclos de formación profesional do Centro. Dado que o campo da xeomática non adoita estar moi cuberto na docencia de informática, seguramente esta xornada sexa unha boa canle introdutoria sobre este campo para todos os asistentes.

Os relatores da xornada teñen un destacado perfil tecnolóxico e unha dilatada experiencia laboral no ámbito do software libre e os sistemas de información xeográfica, polo que os asistentes van poder ver de primeira man as perspectivas laborais que ofrece este ámbito.
