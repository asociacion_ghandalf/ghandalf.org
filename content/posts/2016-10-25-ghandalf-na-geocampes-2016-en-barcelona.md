---
title: "GHANDALF na GeocampES 2016 en Barcelona"
date: "2016-10-25"
cover: "grupo-1024x575.jpeg"
categories: 
  - eventos-publicos
---

![Público na Geocamp](/geocamp-300x158.jpeg) 

E xa van 4... Vigo, A Coruña, Sevilla e este ano [Barcelona](http://geocamp.es/).

Este pasado fin de semana varios membros de Ghandalf participaron na GeocampES que tivo lugar en Barcelona. Esta foi a cuarta edición dun evento que naceu impulsado por Ghandalf e o grupo de [Xeoinquedos galego](http://xeoinquedos.eu/), e que entre todos os grupos de xeoinquedos nacionais tomou forma de evento nacional, converténdose no evento de referencia para estes. Despois das primeiras edicións galegas en Vigo e A Coruña o ano pasado excedeu as fronteiras celebrándose en Sevilla, este ano en Barcelona, e xa se fala da próxima GeocampES 2017 en Almería.

A GeocampES é unha xeo-desconferencia, un evento no que todos os participantes toman un papel máis activo dentro da creación do mesmo. Ten a súa orixe no concepto de Barcamp. É un día para o intercambio de experiencias, coñecementos e valores en torno a todo aquelo que ten que ver co XEO e o non tan XEO, pero sempre arredor do software libre.

![Poñentes da GeocampES](/ponentes-300x300.jpg)

Este ano houbo un total de 12 charlas temáticas nas que se falou de [CARTO](http://carto.com), o proxecto de moda para a visualización de datos a través de mapas e que está a virar cara o mundo da Location Intelligence, con clientes en todo o mundo. Falouse do proxecto [MissingMaps](http://www.missingmaps.org/es/), orientado á axuda humanitaria e relacionado con [OpenStreetMap](http://openstreetmap.org), do que tamén se falou. Presentouse o proxecto [What3Words](http://what3words.com/es/), que permite a xeocodificación en lugares que non teñen definido un sistema de direccións postais e presentáronse tamén as novidades do proxecto [Mapillary](https://www.mapillary.com/), unha alternativa a Google Stretview con fotos colaborativas. Mesmo houbo tempo tamén para a presentación do [libro libre sobre Sistemas de Información Xeográfica de Víctor Olaya](http://volaya.github.io/libro-sig/).

Vémonos o próximo ano en Almería.