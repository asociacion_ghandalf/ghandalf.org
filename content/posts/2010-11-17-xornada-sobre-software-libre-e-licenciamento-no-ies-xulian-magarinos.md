---
title: "Xornada sobre Software Libre e Licenciamento no IES Xulián Magariños"
date: "2010-11-17"
categories: 
  - "formacion"
  - "general"
---

O pasado 2 de novembro, a Asociación GHANDALF impartiu unha xornada de divulgación e formación dirixida aos alumnos dos Ciclos Superiores de Informática do [IES Xulián Magariños de Negreira](http://www.edu.xunta.es/centros/iesxulianmagarinos/).

A temática da devandita xornada foi "_Software Libre e Licenciamento_", tendo como obxectivo amosarlle a panorámica xeral do licenciamento (propiedade intelectual, dereitos de autor, licenzas) e a súa fortísima relación co Software Libre, facendo fincapé naquelas cuestións de máis interese dende o punto de vista de toma de decisións estratéxicas por parte dun prescritor de tecnoloxía, o cal con toda seguridade vai ser o rol de moitos dos asistentes á Xornada.

Sobre o fío conductor anterior, os alumnos dos Ciclos Superiores de Informática (en torno a uns 30 asistentes) foron presentando múltiples dúdidas demostrando tanto o seu interese, como a aplicabilidade deste tópico na súa xa inminente vida laboral.

Ghandalf fíxolle chegar a todos os alumnos un bo conxunto de documentación dixital sobre a temática en cuestión, a maiores de varias ferramentas e sistemas operativos Software Libre.

A presentación empregada na Xornada está dispoñible na [sección de documentación da nosa web](http://www.ghandalf.org/documentacion/), sendo os seguintes os seus enlaces directos:

- En pdf, dispoñible neste enlace directo.
- En odp, dispoñible neste enlace directo.

Impartir esta xornada foi unha agradable labor para Ghandalf, polo que queremos amosarlle o noso agradecemento á Dirección do Centro, a cal en todo momento facilitou que a xornada puidera levarse a cabo dun xeito óptimo.
