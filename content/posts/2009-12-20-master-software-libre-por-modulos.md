---
title: "O Master de Software Libre xa se pode cursar por módulos"
date: "2009-12-20"
categories: 
  - "formacion"
  - "general"
tags: 
  - "formacion"
  - "master"
---

O Master de Software Libre organizado pola empresa galega [Igalia](http://www.igalia.com/) e a Universidade Rey Juan Carlos xa permite cursar algúns módulos de xeito independente. [A inscrición xa está aberta](http://www.mastersoftwarelibre.com/?p=339) para os seguintes módulos:

- [Administración e Integración de Sistemas con Software Libre](http://www.mastersoftwarelibre.com/?page_id=242)
- [Desarrollo en entornos desktop/mobile con Software Libre](http://www.mastersoftwarelibre.com/?page_id=273)
- [Desarrollo en entornos web con Software Libre](http://www.mastersoftwarelibre.com/?page_id=268)

Cada un dos  módulos poderase cursar dende 980€ grazas a política de descontos do Master coma por exemplo ser socio dalgúns GULs entre eles Ghandalf.
