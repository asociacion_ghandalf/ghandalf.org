---
title: "A Asociación GHANDALF inicia a Convocatoria Pública para a concesión de 2 Bolsas para desenvolvemento de PFCs Software Libre"
date: "2010-06-02"
categories: 
  - "formacion"
  - "general"
tags: 
  - "pfc-bolsa"
---

A Asociación GHANDALF, ao abeiro do Convenio 2010 entre as Asociacións Galegas de Usuarios de Software Libre e maila Fundación para o Fomento da Calidade Industrial e o Desenvolvemento Tecnolóxico de Galicia, inicia o proceso de concesión de dúas Bolsas a estudantes universitarios de disciplinas técnicas relacionadas co Software (Enxeñería Informática, Telecomunicacións, Industriais e relacionados) para que o Proxecto Fin de Carreira (PFC) que leven a cabo desenvolva sistemas/aplicacións Software Libre, ou aporte valor técnico no ecosistema do Software Libre.

A intención desta actividade é darlle apoio e orientación técnica a estudantes para que o Proxecto Fin de Carreira que desenvolvan estea imbuído pola filosofía do Software Libre, e ao mesmo tempo poida aportar o seu gran de area no enriquecemento do ecosistema galego do Software Libre.

As bases da convocatoria, e o procedemento para concorrer na mesma, achéganse conxuntamente con esta nova. Só tes que premer neste [enlace](http://www.ghandalf.org/wp-content/uploads/BasesBolsasPFC_SwL_2010.pdf) para ver as bases da convocatoria, ou ben neste outro [enlace](http://www.ghandalf.org/?attachment_id=229) se queres deixar comentarios sobre o texto da propia convocatoria.

Se estás a voltas co PFC... anímate a participar!
