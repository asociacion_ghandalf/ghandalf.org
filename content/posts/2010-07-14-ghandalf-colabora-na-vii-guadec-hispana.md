---
title: "Ghandalf colabora na VII GUADEC-Hispana"
date: "2010-07-14"
categories: 
  - "eventos-publicos"
tags: 
  - "eventos-publicos"
---

Os vindeiros dias 22 e 23 de Xullo terá lugar na [Facultade de Informática da Coruña](http://www.fic.udc.es) a [VII GUADEC-Hispana](http://2010.guadec.es). Este evento reunirá á comunidade de desenvolvedores e usuarios de fala hispana do proxecto [GNOME](http://www.gnome.org). O programa de este ano inclúe moitos temas interesantes para os asistentes que queiran coñecer como empezar a participar nunha comunidade de Software Libre. Pero tamén permite coñecer as últimas novidades do proxecto GNOME e canto á súa aplicación a tecnoloxías móbiles, estudos da súa comunidade, melloras en acecesibilidade, as implicacións dos novos navegadores ou o soporte de sistemas Multitouch, entre outros moitos temas. O [programa](http://2010.guadec.es/guadec/programa) pódese consultar na web do evento.

A organización corre a cargo da asociacións [GNOME Hispano](http://www.gnomehispano.org/) e [GPUL](http://www.gpul.org) e contará coa colaboración de Ghandalf, que volverá a traballar man a man con GPUL como xa fixo na pasada [DudesConf](http://www.dudesconf.org/). Ademáis de na organización do evento Ghandalf participará nel activamente xa que un dos seus membros foi conviado como relator dunha das ponencias.

O rexistro do evento está dispoñible a través do seguinte [formulario](http://2010.guadec.es/guadec/registro).
