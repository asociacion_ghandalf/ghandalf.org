---
title: "Charlas de OpenStreetMap en centros de ensino galegos"
date: "2016-10-07"
categories: 
  - "formacion"
---

[![openstreetmap_logo-svg](images/Openstreetmap_logo.svg_-150x150.png)](http://www.ghandalf.org/wp-content/uploads/2016/10/Openstreetmap_logo.svg_.png)O pasado martes varios membros de Ghandalf votámonos á estrada para dar varias charlas en centros de ensino galegos.

Pola maña estivemos en Coristanco, no CPI Alcalde Xosé Pichel, e pola tarde en Negreira, no IES Xulián Magariños.

En ambos casos a temática das charlas era [OpenStreetMap](http://osm.org), cunha introdución á información xeográfica e as vantaxes dos mapas para transmitir información, así como a importancia de que os datos, igual que o software, sexan libres. Un total de 85 alumnas e alumnos en Coristanco e algo máis de 100 en Negreira puideron coñecer as vantaxes da información xeográfica e o proxecto OpenStreetMap de primeira man, vendo algúns exemplos do seu uso e unha demostración de como editar información neste mapa colaborativo.

Agora, todas e todos eles teñen traballo por diante, xa que preparamos un pequeno concurso para que durante as dúas próximas semanas consigan ser os que máis contribucións fagan a OSM no seu centro.

Temos que agradecer a boa acollida e facilidades que nos deron en ambos centros para poder realizar esta actividade e achegar este proxecto de software libre ás alumnas e alumnos.

Esta actividade realizouse no abeiro do convenio de colaboración asinado coa [AMTEGA](http://amtega.xunta.gal) para a difusión do Software Libre.
