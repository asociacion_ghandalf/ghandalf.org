---
title: "O 17 de novembro celébrase na Ágora da Coruña a OSHWDEM, xornada aberta e demostrativa sobre Open Source Hardware onde poderás atopar robots, impresoras 3D e electrónica para todos os públicos"
date: "2012-11-08"
categories: 
  - "eventos-publicos"
  - "formacion"
  - "general"
tags: 
  - "formacion"
  - "impresoras-3d"
  - "oshw"
---

- Vai ser un evento aberto e gratuíto de carácter tecnolóxico con un formato participativo e innovador, centrado no Open Source Hardware, ou Hardware Libre.
    
- Retoma o testigo da Arduino Barcamp organizada por [Inestable](http://www.inestable.org/) no ano 2011 e dá continuidade a eventos similares como a [OSHWCON](http://oshwcon.org/) celebrado en Madrid en Setembro deste mesmo ano.
    
- Promovida pola Asociación [GHANDALF](http://www.ghandalf.org/), e organizada polo grupo de traballo [BricoLabs](http://www.ociorum.org/web/info/bricolabs), conta coa colaboración de asociacións de Usuarios de Software Libre como [Inestable](http://www.inestable.org/), axentes do mundo do OSHW como [Clonewars](http://www.reprap.org/wiki/Proyecto_Clone_Wars) ou [RadikalBytes](https://twitter.com/radikalbytes), así como das empresas [Farnell](http://farnell.com/) e [Bricogeek](http://www.bricogeek.com/shop/).
    

Se estás interesado no Open Source Hardware, en Arduino, nas impresoras 3D, en GNU/Linux embebido, en montaxe de circuítos electrónicos, nos robots ou na electrónica en xeral, tes unha cita o vindeiro 17 de novembro no [Centro Cultural Ágora](http://www.coruna.es/centroagora) da Coruña.

A OSHWDEM vai ser un evento de carácter tecnolóxico e innovador, relacionado coa "[OSHWCon 2012 Madrid - Open Source Hardware, Electronics & Robotics Convention 2012](http://oshwcon.org/es/informacion)" a cal nas súas diferentes edicións ten demostrado levantar grande interese, esgotando as prazas nos días previos ao evento.

A xornada vai a combinar charlas divulgativas con espazos demostrativos, abordando diferentes temáticas relacionadas co Hardware Libre: Arduino, Raspberry Pi, impresoras 3D, montaxe de circuítos electrónicos, instrumentos musicais libres, deseño 3D con ferramentas libres ou robots libres. Pola tarde realizaranse obradoiros de montaxe de circuítos electrónicos e deseño 3D, nos que os asistentes poderán meter as mans na masa.

Na organización da OSHWDEM están a traballar Asociacións Galegas de Usuarios de Software Libre como GHANDALF ou Inestable, contando co coñecemento e saber facer de referentes do mundo do OSHW como BricoLabs, Clonewars ou Radikalbytes.

A colaboración de empresas do sector como Farnell ou Bricogeek farán que ninguén marche da xornada coas mans baleiras, e incluso haberá agasallos para os asistentes máis participativos.

Ó longo da xornada, así como durante o xantar, contaremos con espazos de networking, coa finalidade de poñer en contacto aos diferentes actores interesados no Hardware Libre.

As empresas, administracións, universidades ou mesmo profesionais independentes, interesados no Open Source Hardware poden atopar na OSHWDEM proxectos do seu interese, ou incluso contar con un espazo para amosar no que están a traballar. Contactade connosco a través do correo oshwdem@gmail.com e vemos como colaborar.

A entrada para a OSHWDEM é de balde, aínda que polo aforo e para participar nos concursos é preciso que te inscribas previamente na [páxina do evento](http://oshwdem.wordpress.com/inscripcion/).

Para estar ao tanto de todas as novidades, tes á túa disposición tanto a web oficial [oshwdem.wordpress.com](http://oshwdem.wordpress.com/) como a conta de twitter [@OSHWDEM](https://twitter.com/OSHWDEM) así como o enderezo de correo electrónico [oshwdem@gmail.com](mailto:oshwdem@gmail.com).

NOTA: Fotos publicables en [http://oshwdem.wordpress.com/fotos-prensa/](http://oshwdem.wordpress.com/fotos-prensa/)
