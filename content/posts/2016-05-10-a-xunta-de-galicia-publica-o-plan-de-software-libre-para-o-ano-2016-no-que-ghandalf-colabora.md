---
title: "A Xunta de Galicia publica o Plan de Software Libre para o ano 2016 no que GHANDALF colabora"
date: "2016-05-10"
categories: 
  - "general"
  - "vida-asociativa"
---

Este mes de maio [publicouse o Plan de Software Libre para o ano 2016 da Xunta de Galicia](http://www.mancomun.gal/no_cache/actualidade/novas/detalledenova/nova/publicase-o-plan-de-solftware-libre-2016-coa-colaboracion-dos-principais-axentes-do-sector/). Este plan define as liñas de actuación da administración nesta materia.

Ao mesmo tempo e como vén sendo habitual dende fai varios anos, GHANDALF asinou coa [Axencia de Modernización Tecnolóxica de Galicia](http://amtega.xunta.gal) (AMTEGA), xunto con outras asociacións un convenio de colaboración para a realización durante todo este ano de diferentes actividades de difusión do Software Libre.

Entre as actividades previstas ao abeiro deste convenio por parte da nosa asociación podemos destacar a celebración de diferentes eventos, colaborando con diversas asociacións, como son a [OSHWDem](http://oshwdem.org) xunto á xente de [Bricolabs](http://bricolabs.cc/), a [GeocampES](http://geocamp.es/) con [Geoinquiets](https://geoinquiets.cat/), o unha xornada sobre Python coa xente de [Python Vigo](https://www.python-vigo.es/). Seguiremos tamén coa importante difusión do Software Libre en centros de ensinanza con [charlas de introdución ao Software Libre](http://www.mancomun.gal/no_cache/actualidade/novas/detalledenova/nova/un-ano-mais-a-asociacion-ghandalf-achega-o-software-libre-as-novas-xeracions-impartindo-relator/) e a [cartografía libre con OpenStreetMap](http://psanxiao.com/hablando-de-ig-y-openstreetmap-en-el-ies-xulian-magarinos), coa distribución de copias impresas das nosas publicacións, e como en Ghandalf temos un xen moi [Xeoinquedo](http://xeoinquedos.eu/), este ano tentaremos facer algo relacionado con _drons_ e [OpenStreetMap](http://osm.org) que nos ten moi motivados e que pouco a pouco iremos desvelando.
