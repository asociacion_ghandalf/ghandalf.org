---
title: "Participación de GHANDALF no V Curso de Tecnoloxía e Cooperación para o Desenvolvemento Humano"
date: "2010-04-22"
categories: 
  - "eventos-publicos"
  - "general"
tags: 
  - "formacion"
---

O [V Curso de Tecnoloxía e Cooperación para o Desenvolvemento Humano](http://www.galicia.isf.es/tcpdh/index.php) é unha iniciativa de [Enxeñería Sen Fronteira Galicia](http://www.galicia.isf.es/home/index.php) en colaboración coa [Universidade de Santiago de Compostela](http://www.usc.es) que persegue mostrar as/aos alumnas/os unha visión global das causas e consecuencias da pobreza e da desigualdade no mundo e a necesidade de xuntar esforzos e adquirir un compromiso activo para poder combater esas situacións. No discorrer do curso vaise facer fincapé na importancia de por a tecnoloxía e a enxeñería a servicio do desenvolvemento, entendéndoas e aplicándoas dentro do entorno cultural, político, social, económico e ambiental en que se empregan. A nosa Asociación recibiu a honra de participar nestas xornadas a través da ponencia que vai a levar a cabo Andrés Estévez (membro do Grupo de Sistemas de Información de Enxeñería Sen Fronteiras Galicia e membro de GHANDALF) titulada "Tecnoloxías da información para a Comunicación (TICs). O caso do Software Libre". A devandita ponencia está reprogramada para o venres 23 de abril entre as 18:45 e as 20:00 horas no campus sur da Universidade de Santiago de Compostela.
