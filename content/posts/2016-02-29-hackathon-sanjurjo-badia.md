---
title: "Hackathon Sanjurjo Badia"
date: "2016-02-29"
categories: 
  - "eventos-publicos"
---

![michogar no Hackathon Sanjurjo Badia](images/12718051_10206314831051901_740406239664926622_n-300x225.jpg)
Este pasado fin de semana, tivo lugar en Vigo, o [Hackathon Sanjurjo Badia,](http://hacksb.vigolabs.gal/es/) organizado por [Eloy Coto](https://twitter.com/eloycoto) e [Jesús Sayar,](https://twitter.com/jsayar)  o evento reuniu a 80 persoas que estiveron a traballar durante os dous días que durou o mesmo. A idea deste hackathon era a de crear proxectos de Internet of Things (IoT) relacionados co mar. Agrupados por equipos nun ambiente de colaboración pero tamén de competición, os participantes contaban cunha placa UDOO Neo, un combinado de RaspberryPi, Arduino, sensores e conectividade, un kit moi completo que permite executar tanto GNU/Linux como Android.

Paralelamente á competición tiveron lugar diversos obradoiros sobre tecnoloxías libres que se foron celebrando durante todo o fin de semana, complementando o evento. Un destes obradoiros foi impartido polo noso compañeiro Micho García ([@michogar](https://twitter.com/michogar)), que impartiu unha sesión de introdución a [PostgreSQL](http://www.postgresql.org.es/) e ás súas capacidades espaciais coa extensión [PostGIS](http://postgis.net/).

Tamén estivo por alí outro dos nosos compañeiros, [Fran Puga](http://conocimientoabierto.es/), participando nun dos equipos, aínda que finalmente non puido levarse a vitoria, que recaeu no equipo do proxecto iNasa, que busca crear una nasa intelixente coa que optimizar esta arte de pesca.

### Sanjurjo Badía

Quen pon nome a este Hackathon, foi un ilustre inventor galego que viviu en Vigo e que tivo unha grande relación co mar. Emigrante en Cuba, montou en Vigo primeiro uns talleres e despois uns estaleiros, famosos polas súas innovacións. Entre os seus inventos destaca un submarino que en 1898 foi capaz de aguantar hora e media mergullado, con vistas a ser empregado na guerra contra a flota estadounidense, pero finalmente non foi empregado ao firmarse o tratado de paz entre ambos países.
