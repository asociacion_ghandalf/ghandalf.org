---
title: "10 anos de Arduino. Arduino Day 2014"
date: "2014-03-24"
categories: 
  - "eventos-publicos"
---

O 29 de Marzo celébrase a nivel global un evento de celebración dos 10 anos de Arduino, plataforma de Hardware Libre creada para facilitar a creación de proxectos interactivos.

A asociación makers.lugo, da que forman parte algúns membros de Ghandalf, uníndose ás celebracións globais do Arduino Day 2014 #ArduinoD14 organiza un evento aberto e para tódolos públicos, que terá lugar na Casa da Xuventude, Rúa Xesús Bal e Gay, a mañá do sábado 29 de Marzo, de 10:30 a 13:30.

Ó longo da mañá, membros das asociacións makers.lugo e Bricolabs, estarán dispoñibles para resolver dúbidas para quen desexe introducirse no mundo de Arduino, axudar a aqueles que desexen empezar a facer proxectos. Tamén haberá un espacio onde tódolos participantes poderán amosar os seus proxectos realizados con Arduino.

Máis información: • Arduino - [http://arduino.cc/](http://arduino.cc/ "http://arduino.cc/") • #ArduinoD14 - [http://day.arduino.cc/](http://day.arduino.cc/ "http://day.arduino.cc/") • Asociación makers.lugo - [http://makerslugo.odiseus.org/](http://makerslugo.odiseus.org/ "http://makerslugo.odiseus.org/") • Asociación Bricolabs - [http://bricolabs.cc/](http://bricolabs.cc/ "http://bricolabs.cc/") • Información sobre o evento en Galicia, Lugo - [http://makerslugo.odiseus.org/evento/10-anos-de-arduino/](http://makerslugo.odiseus.org/evento/10-anos-de-arduino/ "http://makerslugo.odiseus.org/evento/10-anos-de-arduino/")
