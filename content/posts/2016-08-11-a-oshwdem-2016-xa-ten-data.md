---
title: "A OSHWDem 2016 xa ten data"
date: "2016-08-11"
categories: 
  - "eventos-publicos"
---

![OSHWI-color-low-228x300-228x300](images/OSHWI-color-low-228x300-228x300-150x150.png)
A OSHWDem, que naceu impulsada pola Asociación GHANDALF e organizada dende [BricoLabs](http://bricolabs.cc/) vai celebrar este ano a súa quinta edición. O evento terá lugar o sábado 5 de Novembro na Domus de A Coruña e como sempre virá cargada de novidades. Permanece atento a súa [páxina web](http://oshwdem.org/) para ir coñecendo todas as novidades que terá a edición deste ano.

Dende GHANDALF seguiremos apoiando o evento como en tódalas edicións anteriores, tratando e poñer a Galicia no mapa do [movemento maker](https://es.wikipedia.org/wiki/Cultura_hacedora) durante unhas horas.
