---
title: "Xornada de Cartografía colaborativa con Drons no CEIP Virxe do Carme de Sober"
date: "2019-05-07"
categories: 
  - "formacion"
tags: 
  - "dron"
  - "mapas"
  - "openstreetmap"
cover: "IMG_20190507_191222_507_Fotor-1024x562.jpg"
---

O pasado venres tivemos a sorte de compartir unha xornada coas rapazas e rapaces do [CEIP Virxe do Carmen](http://www.edu.xunta.gal/centros/ceipvirxedocarmesober/) en Sober. A idea era introducilos no mundo da cartografía colaborativa da man do proxecto [OpenStreetMap](http://osm.org) e mostrarlles como os drons nos poden axudar nesta tarefa de cartografiar o mundo empezando polo sitio donde vivimos.

A xornada estivo dividida en tres partes. Comezamos por repasar conceptos básicos sobre os mapas, que elementos conteñen, como se fan e como nos sirven para contar historias. Para iso deixamos que primeiro eles debuxasen un mapa de Sober e sobre iso  empezamos a repasar conceptos con varios exemplos. Iso nos levou a como se fan os mapas hoxe en día e como a tecnoloxía vai mudando a forma de facelos. 

![IMG_20190507_191235_236_Fotor](/IMG_20190507_191235_236_Fotor-300x263.jpg)

Con isto enlazamos a segunda parte que consistiu en facer un voo  co dron para obter unha ortofoto sobre a que poder despois mapear as zonas de Sober mediante OSM. Para compoñer a ortofoto empregamos o proxecto de software libre [WebODM](https://www.opendronemap.org/webodm/).

![IMG_20190507_191224_831_Fotor](/IMG_20190507_191224_831_Fotor-300x225.jpg)

Por último, de novo na aula, ensinamoslles como mapear con OpenStreetMap, empregando como base a ortofoto que fixemos co dron, para que puxesen no mapa os sitios máis importantes do concello, así divididos en grupos foron localizando varios puntos de interese do concello ou mesmo algunha estrada que non estaba mapeada.

Agradecerlle enormemente a todo o profesorado do centro a súa axuda, a [Elías](https://twitter.com/eliasgago) e [Rocío](https://twitter.com/rocioferper) por organizalo, e sobre todo ás rapazas e rapaces, verdadeiros protagonistas da xornada, pola seu recibimento e a súa atención. Da gusto ver como se implican, a súa curiosidade polas cousas novas e como asimilan todos os conceptos en tan pouco tempo. Esperamos que de aquí nuns anos sexan eles quenes nos conten historias cos mapas.
