---
title: "A OSHWDem 2016 acada cifras récord"
date: "2016-11-07"
cover: "gente_rampas-1024x948.jpg"
categories: 
  - "eventos-publicos"
tags: 
  - "eventos"
  - "maker"
  - "oshwdem"
---

![Asistencia na OSHWDem](/gente_rampas-300x278.jpg)
Esta pasada fin de semana celebrouse unha nova edición da [OSHWDem](http://oshwdem.org), que bateu tódolos récords en canto a asistencia e participación.

- Un total de **2150** asistentes.
- **43 proxectos** diferentes expostos na feira
- **_makerspaces de toda Galicia: Recuncho Maker, Makers Lugo, Pontemaker, Clou Wars, Tropa Korriban…_**
- **máis de 100 _makers_** na exposición contando a todo o persoal dos distintos proxectos e charlas

Unhas cifras que fan deste evento un dos máis importantes na escena _maker_ e que fala do bo traballo que está a desenvolver **BricoLabs,** artífice principal da organización coa axuda de GHANDALF.

E a pesar de que o evento principal se celebrase na fin de semana, aínda quedan pendentes varios talleres que se celebrarán nos próximos días e **cuxa inscrición abrirase hoxe luns a partir das 20:00 horas**.

Para GHANDALF é un pracer ver como un evento que impulsamos dende os seus inicios, cando era so unha idea que latexaba na cabeza dun dos nosos socios se converteu xa en todo un referente anual para a cada vez máis numerosa comunidade _maker_.

Un ano máis a colaboración de GHANDALF na organización da OSHWDem está enmarcada dentro da actividades que esta asociación vén realizando ao abeiro do Convenio para a Difusión do Software Libre asinado coa AMTEGA.
