---
title: "Nova web do libro Máster Software Libre en Galicia, Ano II"
date: "2016-03-07"
categories: 
  - "general"
tags: 
  - "master"
  - "software-libre"
---

![libro-mswl2](images/libro-mswl2.png)
Publicamos unha nova [páxina web](http://mswl2.ghandalf.org/) adicada ao libro Máster Software Libre en Galicia, Ano II, libro, que editamos dende a Asociación GHANDALF fai xa uns anos, e que recolle os traballos fin de mestrado dos alumnos da segunda edición do [Máster de Software Libre,](http://www.mastersoftwarelibre.com/) traducidos ao galego.

Dende GHANDALF, cremos na importancia da difusión do Software Libre, así como na importancia de dispoñer de documentación en galego que achegue esta o coñecemento aberto á nosa terra. Por iso, dende o principio, unha das nosas principais actividades foi a da edición de libros sobre Software Libre na nosa lingua, comenzado pola tradución dun libro de referencia como foi [Creando Software Libre](http://producingoss.ghandalf.org/), de [Karl Fogel](http://producingoss.com/), e as ata o de agora dúas publicacións dos traballos da [primeira](http://mswl.ghandalf.org/) e segunda edición do Máster.

Nesta nova páxina web pódese atopar un pouco de historia do libro, quen participou, os prólogos e epílogos que diferentes entidades nos fixeron para o mesmo, e o máis importante, os enlaces para a súa descarga, en formato libre e editable, xa que o mesmo se publica baixo licenza _creative commons_.

Para rematar, salientar que o desenvolvemento desta nova páxina web, de igual xeito que no seu momento a edición da versión impresa, foi posible grazas ao convenio de colaboración asinado coa [AMTEGA](http://amtega.xunta.gal) para a difusión do Software Libre.
