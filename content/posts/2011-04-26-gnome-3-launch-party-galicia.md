---
title: "Gnome 3 Launch Party Galicia"
date: "2011-04-26"
categories: 
  - "eventos-publicos"
  - "general"
tags: 
  - "gnome"
---

Co lanzamento da [versión 3](http://www.gnome3.org/), o [proxecto Gnome](http://www.gnome.org/) dá un enorme salto cualitativo tanto dende o punto de vista tecnolóxico, como dende o punto de vista funcional. Deste xeito, a versión 3 representa a nova xeración do escritorio libre por excelencia.

Para difundir a trascendencia deste lanzamento, o vindeiro xoves 28 de abril na aula 3.1 da Facultade de Informática da Universidade da Coruña, celebraremos a Gnome 3 Lauch Party galega.

Para profundizar axeitadamente en Gnome3, temos preparado o seguinte programa:

- **18:00 - 18:15    Presentación da GNOME 3 Launch Party Galicia**
- **18:15 - 18:45    Presentación do proxecto GNOME**
- **18:45 - 19:15    Demo de GNOME 3**
- **19:15 - 19:45    GNOME 3  galeguizado**
- **19:45 - 20:00    Sorteo de agasallos entre os asistentes, incluíndo un portátil con GNOME 3!!**

A Launch Party galega está sendo organizada pola Asociación Ghandalf en colaboración coas Asociacións Gpul e Trasno, contando co patrocinio da Xunta de Galicia a través do Convenio 2011 coas Asociacións Galegas de Usuarios de Software Libre.

En base ao programa exposto, percorreremos a interesante historia do proxecto Gnome, amosando as importantes innovacións presentes tras do recente salto xeracional, para a continuación ver en acción o que Gnome 3 nos ofrece, e coñecer de primeira man o intenso traballo da comunidade galega de localización de Gnome, a cal posibilitou que todos nós poidamos disfrutar de Gnome 3 en galego dende o primeiro día.

Para rematar a launch party, repartiremos agasallos entre todos os asistentes. A maiores das camisolas, chapas, tazas, etc. conmemorativas do lanzamento, **tamén podes conseguir un portatil con Gnome 3**, listo para que poidas empregar a última versión deste exitoso escritorio libre!

![Cartel da Gnome 3 Launch Party Galicia](/Gnome3LaunchPartyGalicia.png)
