---
title: "Ghandalf e Xeoinquedos organizamos a primeira Geocamp ES"
date: "2013-06-07"
categories: 
  - "eventos-publicos"
---

O mundo das tecnoloxías de información xeográfica e o software libre están a ter cada vez un peso máis grande no mundo no que vivimos, tanto dende o punto de vida persoal como profesional. Nos últimos anos estase a vivir un movemento de xente con grandes inquedanzas sobre estos temas e están a aparecer grandes profesionais do sector que falan Galego e que participan en proxectos e xornadas de caracter nacional e internacional.

A partires dunha destas participacións, concretamente na Geocamp do ano 2012 en Campo Maior (Portugal) nace a idea de crear un evento no que compartir experiencias e coñecemento sobre temas “xeo” e software libre en Galicia.

Organizada por un grupo de xeoinquedos galegos e maila asociación GHANDALF, a [Geocamp](http://geocamp.es/) que se fará en formato de “desconferencia” tenta favorecer a participación e a colaboración, xa que serán os propios participantes no evento os que conforme o programa de charlas o propio día do mesmo, xusto antes de comenzar. Por suposto está garantida a participación de grandes nomes no mundo do software libre e da xeomática a nivel nacional e internacional.

Esta primeira edición celebrarase na localidade de Bouzas, que pertence ao municipio de Vigo, fuxindo así das grandes cidades e apostando tamén por dar a coñecer as pequenas vilas galegas e os seus encantos.

A entrada é de balde, así como a comida e os cafés que se servirán nos descansos, haberá agasallos para os asistentes e algunha sorpresa final. Todo isto grazas aos patrocinadores do evento.

Para estar atento as novidades está dispoíble unha web do evento, así como perfís de twitter(@geocampes) e [facebook](https://www.facebook.com/geocampes).
