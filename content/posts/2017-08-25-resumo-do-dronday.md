---
title: "Resumo do DronDay"
date: "2017-08-25"
cover: "DCgi2ZuWAAAjc5j.jpglarge-1024x576.jpeg"
categories: 
  - "eventos-publicos"
---

O pasado mes de Xuño, Ghandalf e Xeoinquedos organizamos o primeiro [DronDay](http://xeoinquedos.eu/dronday/) en Galicia, onde contamos coa axuda da xente do proxecto proxecto [DroneMapZ](http://cesaretopia.com/dronemapz/), quenes viñeron dende Zaragoza para amosarnos a súa experiencia no uso de drons para mapeado.

A xornada empezou pola maña para facer unha demostración de voo con dous drons diferentes, un modelo comercial e un modelo _maker_, construído a partires dun placa [ArduPilot](http://ardupilot.org/), de hardware libre. Con ambos aparatos fixéronse vairos voos en dúas ubicacións diferentes nos arredores de Vigo, facendo tanto voos manuais como automáticos, programando os drons para sacar fotografías dunha área determinada, que posteriormente se ían empregar para xerar imaxes xeorreferenciadas coas que poder editar diferentes elementos en [OpenStreetMap](http://osm.org).

![](/DCh5SQuXgAQbo0s.jpglarge-300x225.jpeg)

Xa pola tarde, e grazas á colaboración de [Kaleido](http://www.kaleidocoworking.com/) que nos deixou as súas instalacións, fíxose un taller no que se mostrou aos asistentes como procesar as fotografías tomadas cos drons para conseguir con elas imaxes xeoreferenciadas. Este proceso realizouse coa ferramenta de software libre [WebODM](http://docs.webodm.org/), un proxecto que permite xerar tanto imaxes 2D xeoreferenciadas, como modelos 3D con nubes de puntos.

Un dos obxectivos a acadar era mediante esta aplicación conseguir xerar imaxes que puidesen ser cargadas no proxecto OpenStreetMap para sobre elas, editar o proxecto engadindo información xeográfica dos elementos que se fotografiaron cos drons. Desta maneira, empregando un dron pódense obter imaxes actualizadas para crear cartografía libre e non depender das fotografías por satélite das grandes compañías e dos seus fluxos de actualización.

![](/DCibaCPXkAAreWe.jpglarge-300x225.jpeg)

Finalmente, este obxectivo foi acadado con éxito, e os asistentes puideron rematar o obradoiro cargando as imaxes dos voos da maña nas súas contas de OpenStreetMap, listas para poder editar sobre elas. En breve publicaremos con licenza libre tanto as imaxes obtidas nos voos como os resultados procesados con WebODM, permanecede atentos.

Por último queremos agradecer especialmente a Fran e Xosé Luis por vir ata Galicia a compartir parte do seu coñecemento nesta materia e a [AMTEGA](https://amtega.xunta.gal/) por financiar esta actividade ao abeiro do Convenio para a Difusión do Software Libre en Galicia asinado coas Asociacións Galegas de Usuarios de Software Libre.
