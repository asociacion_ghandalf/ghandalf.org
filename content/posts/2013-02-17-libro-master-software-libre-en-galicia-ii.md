---
title: "Libro \"Máster Software Libre en Galicia, ano II\""
date: "2013-02-17"
categories: 
  - "general"
---

No ano 2012 a Asociación GHANDALF elaborou o libro "**Máster Software Libre en Galicia, ano II**", sendo o mesmo unha escolma dos principais traballos levados a cabo polos alumnos deste Máster na súa edición galega do curso 2008-2009. Deste xeito, inclúe os traballos de:

Manuel Fontán García Pedro García Rodríguez Javier Jardón Cabezas Beatriz Montero Fernández José Puente Fuentes Manuel Rego Casasnovas

A maiores dos contidos citados, o libro contra cun prólogo xentilmente elaborado pola Asociación de empresas galegas de software libre (AGASOL) e con epílogo obra do GPUL (Grupo de Programadores e Usuarios de Linux).

O traballo de elaboración do libro foi levado a cabo pola Asociación GHANDALF, sendo Roberto Vieito o encargado da coordinación e dirección das diferentes tarefas precisas para a súa confección.

A falta de rematar unha web propia para este libro na que se inclúan os ficheiros fonte e mailas diversas versións do mesmo, ao estilo de [producingoss.ghandalf.org](http://producingoss.ghandalf.org/) ou [mswl.ghandalf.org](http://mswl.ghandalf.org/), no presente artigo poñemos a disposición de calquera interesado-a este novo libro técnico en galego sobre o Software Libre. Pódelo descargar dende  esta [ligazón](http://www.ghandalf.org/wp-content/uploads/2013/02/MasterSoftwareLibreGaliciaAnoII_formatoLibro.pdf).
