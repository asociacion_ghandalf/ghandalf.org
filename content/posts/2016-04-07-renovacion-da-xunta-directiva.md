---
title: "Renovación da Xunta Directiva"
date: "2016-04-07"
categories: 
  - "general"
  - "vida-asociativa"
---

Despois de dous anos, este pasado mes de marzo tocou renovación da Xunta Directiva da asociación. Na asemblea celebrada só se presentou unha candidatura que foi aprobada polos socios quedando conformada a nova xunta directiva como segue:

- Presidente: Roberto Vieito Raña
- Secretario: Pablo Sanxiao Roca
- Tesoureiro: Francisco Puga Alonso
- Vogal: Juan Ignacio Varela García

A nova xunta será a encargada de potenciar as actividades de difusión en prol do Software Libre que como cada ano se realizan dende a asociación. Para este ano estase xa a definir o programa das mesmas que terán por unha banda un carácter continuista, apostando pola celebración dunha edición máis da [OSHWDem](http://oshwdem.org/), tamén da [GeocampES](http://geocamp.es), as charlas sobre Software Libre e [OpenStreetMap](http://osm.org) en institutos de ensinanza de Galicia ou a difusión de exemplares dos libros publicados pola asociación ata o momento.

Pero ademais, este ano estase a dar forma a novas actividades que se desvelarán moi pronto pero que podemos adiantar que terán que ver con Python, buscando colaboracións con novas organizacións e tamén cos tan de moda _drons_, tentando de analizar novas posibilidades que estos aparellos poden ofrecer para compartir información.
