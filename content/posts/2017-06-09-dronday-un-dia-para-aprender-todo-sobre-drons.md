---
title: "DronDay, un día para aprender todo sobre drons"
date: "2017-06-09"
categories: 
  - "eventos-publicos"
tags: 
  - "dron"
  - "openstreetmap"
  - "software-libre"
---

Dende fai un tempo na Asociación estamos a investigar sobre as posibilidades de uso de drons e software libre para obter imaxes aéreas que poidan servir como cartografía base para certos proxectos e ser empregadas xunto con [OpenStreetMap](http://openstreetmap.com). Para iso contamos coa colaboración de [Xeoinquedos](http://xeoinquedos.eu) e xuntos estivemos a explorar varias opcións ata que contactamos coa xente do proxecto [DroneMapZ](http://cesaretopia.com/dronemapz/) de Zaragoza, quenes levaban xa tempo explorando tamén esta vía.

Froito destes contactos o vindeiro **sábado 17 de Xuño organizamos en Vigo** unha xornada que denominamos DronDay, onde grazas a Fran e Xosé Luis faremos unha introdución a todas estas tecnoloxías aberta ao público. Pola maña faremos varios voos con diferentes drons para ver como se poden obter imaxes aéreas dunha zona, e despois, xa pola tarde veremos como procesar todas esas imaxes, con software libre, para poder empregalas como base en OpenStreetMap, dispoñendo así de información sobre a que xerar nova cartografía coas ferramentas de OSM.

Na [páxina web](http://xeoinquedos.eu/dronday/) que fixemos para o evento podedes atopar toda a información en detalle, así como a ligazón para inscribirse no mesmo, xa que polas características da xornada temos prazas limitadas.

Esta actividade forma parte das que dende a Asociación Ghandalf facemos en colaboración coa [AMTEGA](http://amtega.gal), a través do convenio que asinamos para a difusión do software libre en Galicia.
