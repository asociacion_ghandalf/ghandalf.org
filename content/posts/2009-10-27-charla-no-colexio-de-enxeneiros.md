---
title: "Charla para o Colexio de Enxeñeiros Industriais de Galicia"
date: "2009-10-27"
categories: 
  - "eventos-publicos"
  - "general"
---

GHANDALF realizará este venres no [Colexio de Enxeñeiros Industriais de Galicia, delegación de Vigo](http://www.icoiig.es/delegaciones.html) de 19:00 a 21:00h unha charla de _Introdución ó Software Libre e ó Sistema Operativo Ubuntu_. Durante a sesión, tratarase de que os asistentes comprendan o concepto do Software Libre así como de amosar o uso e estado do Software Libre no escritorio practicando co sistema operativo Ubuntu.

### **Introdución**

- Qué é o Software Libre?
- Por qué é útil para empresas, administracións e usuarios?
- Proxectos de Software Libre:
    - Nokia e Maemo
    - A Generalitat Valenciana e GvSIG
    - Linux e a comunidade que ten ó seu arredor

### **Módulos de Uso do Sistema**

_Básico (uso ofimático)_

- Primeira Vista do Sistema
- Instalación de programas
- Conceto de escritorios
- Multimedia, Internet
- OpenOffice

_Avanzado_

- Virtualización e uso de varios sistemas.
- Compartición de recursos entre diferentes sistemas operativos
- Sincronización e backups.
- Uso de consola e automatización de tarefas.
- Caso específico: uso de GIS

[Software Libre: charla no colexio de enxeñeiros](http://www.slideshare.net/amaneiro/software-libre-charla-no-colexio-de-enxeeiros "Software Libre: charla no colexio de enxeñeiros")

View more [documents](http://www.slideshare.net/) from [Andrés Maneiro](http://www.slideshare.net/amaneiro).
