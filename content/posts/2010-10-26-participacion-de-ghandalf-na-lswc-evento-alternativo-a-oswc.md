---
title: "Participación de GHANDALF na LSWC (evento alternativo á OSWC)"
date: "2010-10-26"
categories: 
  - "general"
---

Tras da cancelación, por parte da Junta de Andalucía, da OSWC, as empresas que forman ASOLIF (Federación de Asociacións de Empresas de Software Libre de España) deron unha tremenda sinal de madurez por parte do tecido empresarial relacionado co Software Libre mediante a organización dun evento substitutivo, a celebrarse no mesmo lugar nas mesmas datas, e co mesmo obxectivo: a difusión e promoción do Software Libre e de casos de éxito do seu uso.

Este evento, nomeado como [Libre Software World Conference](http://libresoftwareworldconference.org) fixo unha convocatoria aberta de participación no mesmo por parte de axentes conformadores do ecosistema español do Software Libre. Ghandalf, aproveitando o traballo xa feito para a OSWC, presentou a súa candidatura a participar no mesmo, e contou co enorme privilexio de ser escollido entre os aspirantes para participar como relatores. Dende aquí, gustaríanos facerlle chegar o noso máis profundo agradecemento a Roberto Brenlla, membro do comité de selección de contidos, polo bo acollemento que tivo da nosa candidatura, e pola valiosa asistencia que nos fixo para que a mesma percorrese exitosamente todos os pasos para ser aprobada.
