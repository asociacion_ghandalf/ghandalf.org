+++
title = "Publicacións"
+++


{{<image style="float: left; padding-right: 50px" src="/libro-producingoss.png">}} 
Proxecto de tradución ao galego do libro Producing Open Source Software de Karl Fogel.
É un libro que versa sobre a parte humana e sobre a parte técnica do desenvolvemento de Software Libre. Describe como xestionar con éxito proxectos de Software Libre, as expectativas dos usuarios e dos desenvolvedores, etc. polo que é unha obra ideal para o achegamento á cultura do software libre.

**Sitio web**: https://producingoss.ghandalf.org

---

{{<image style="float: left; padding-right: 50px" src="/libro-mswl.png">}} 
O libro «Máster Software Libre en Galicia, ano I», escrito polos alumnos da primeira edición galega do Máster en Software Libre, achega unha interesante serie de artigos técnicos e divulgativos sobre o incrible mundo do software libre.  

**Sitio web**: https://mswl.ghandalf.org

---

{{<image style="float: left; padding-right: 50px" src="/libro-mswl2.png">}} 
O libro «Máster Software Libre en Galicia, ano II», escrito polos alumnos da segunda edición galega do Máster en Software Libre, achega unha interesante serie de artigos técnicos e divulgativos sobre o incrible mundo do software libre. 

**Sitio web**: https://mswl2.ghandalf.org

---

{{<image style="float: left; padding-right: 50px" src="/finaeosmapas.jpg">}} 
Libro infantil ilustrado, que explica a importancia dos mapas e os proxectos de mapas colaborativos, poñendo como exemplo OpenStreetMap.

**Sitio web**: https://finaeosmapas.ghandalf.org