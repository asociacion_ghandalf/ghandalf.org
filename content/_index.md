+++
framed = true
+++

A asociación GHANDALF somos una entidade sen ánimo de lucro. Traballamos na divulgación da cultura libre: Software Libre, Hardware Libre e Datos Abertos.
Para acadar este obxectivo artellamos actividades de todo tipo, organizando e colaborando en eventos, obradoiros, faladorios... en calquera sitio onde se difunda a cultura libre podes atopar unha ghandalfeira.

Como asociación galega queremos facer fincapé na difusión da cultura libre na nosa lingua, así como conseguir que haxa máis documentación técnica en Galego, por iso facemos proxectos de edición e tradución de documentación relacionada co Software Libre en Galego. Podes botar un ollo ás nosas publicacións.

Tamén pensamos que é moi importante difundir os valores do Software Libre e a cultura libre entre a rapazada, por iso tratamos de organizar charlas e obradoiros en centros de ensino galegos.

Se quixeres colaborar na mellora da sociedade desde o punto de vista da tecnoloxía, dende Asociación GHANDALF estaremos encantados en prestarche toda a axuda que poidamos.

{{< image src="/GHANDALF_logo.png" alt="Logo Asociación Ghandalf" position="center" style="border-radius: 8px;" >}}